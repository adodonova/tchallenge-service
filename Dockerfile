FROM openjdk:8-jdk-alpine
EXPOSE 8086
ADD ./source/build/libs/*.jar /usr/src/myapp/tchallenge-service.jar
ENTRYPOINT ["java", "-jar", "/usr/src/myapp/tchallenge-service.jar"]
